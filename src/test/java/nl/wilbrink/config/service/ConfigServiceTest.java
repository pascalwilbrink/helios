package nl.wilbrink.config.service;

import nl.wilbrink.config.ConfigItemDTOBuilder;
import nl.wilbrink.config.ConfigItemKey;
import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.config.entity.ConfigItem;
import nl.wilbrink.config.mapper.ConfigItemMapper;
import nl.wilbrink.config.repository.ConfigItemRepository;
import nl.wilbrink.thermostat.ThermostatType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfigServiceTest {

    @Mock
    private ConfigItemRepository configItemRepository;

    @Mock
    private ConfigItemMapper configItemMapper;

    @InjectMocks
    private ConfigService subject;

    @Test
    public void itShouldGetAllConfigItems() {
        ConfigItemDTO configItem1 = ConfigItemDTOBuilder
                .aConfigItemDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withKey(ConfigItemKey.THERMOSTAT_TYPE)
                .withValue(ThermostatType.NEST.name())
                .build();

        ConfigItem entity1 = new ConfigItem();
        when(configItemMapper.toDTO(entity1)).thenReturn(configItem1);

        ConfigItemDTO configItem2 = ConfigItemDTOBuilder
                .aConfigItemDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withKey(ConfigItemKey.TEMPERATURE_UNIT)
                .withValue("Celcius")
                .build();

        ConfigItem entity2 = new ConfigItem();
        when(configItemMapper.toDTO(entity2)).thenReturn(configItem2);

        when(configItemRepository.findAll()).thenReturn(Arrays.asList(entity1, entity2));

        List<ConfigItemDTO> result = subject.getAllConfigItems();

        assertThat(result, containsInAnyOrder(configItem1, configItem2));
    }

    @Test
    public void itShouldGetAConfigItemById() {
        String id = java.util.UUID.randomUUID().toString();

        ConfigItem entity = new ConfigItem();
        ConfigItemDTO dto = new ConfigItemDTO();

        when(configItemRepository.findOne(id)).thenReturn(entity);
        when(configItemMapper.toDTO(entity)).thenReturn(dto);

        ConfigItemDTO result = subject.getConfigItemById(id);

        assertThat(result, equalTo(dto));
    }

    @Test
    public void itShouldGetAConfigItemByKey() {
        ConfigItem entity = new ConfigItem();
        ConfigItemDTO dto = new ConfigItemDTO();

        when(configItemRepository.findOneByKey("THERMOSTAT_TYPE")).thenReturn(entity);
        when(configItemMapper.toDTO(entity)).thenReturn(dto);

        ConfigItemDTO result = subject.getConfigItemByKey(ConfigItemKey.THERMOSTAT_TYPE);

        assertThat(result, equalTo(dto));
    }

    @Test
    public void itShouldUpdateAConfigItem() {
        ConfigItem entity = new ConfigItem();
        ConfigItemDTO dto = new ConfigItemDTO();

        when(configItemMapper.toEntity(dto)).thenReturn(entity);
        when(configItemMapper.toDTO(entity)).thenReturn(dto);

        when(configItemRepository.save(entity)).thenReturn(entity);

        ConfigItemDTO result = subject.updateConfigItem(dto);

        assertThat(result, equalTo(dto));
    }
}