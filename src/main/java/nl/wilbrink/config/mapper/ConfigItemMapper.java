package nl.wilbrink.config.mapper;

import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.config.entity.ConfigItem;
import org.mapstruct.Mapper;

@Mapper
public interface ConfigItemMapper {

    ConfigItemDTO toDTO(ConfigItem entity);

    ConfigItem toEntity(ConfigItemDTO dto);

}
