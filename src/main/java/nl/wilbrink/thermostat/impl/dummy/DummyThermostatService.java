package nl.wilbrink.thermostat.impl.dummy;

import nl.wilbrink.thermostat.dto.ThermostatDTO;
import nl.wilbrink.thermostat.ThermostatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class DummyThermostatService implements ThermostatService {

    private final DummyThermostatMapper mapper;

    @Autowired
    public DummyThermostatService(DummyThermostatMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public List<ThermostatDTO> getThermostats() {
        DummyThermostat entity = new DummyThermostat();
        entity.setId(java.util.UUID.randomUUID().toString());
        entity.setName("Dummy thermostat");
        entity.setCurrentTemperature(20.0f);
        entity.setTargetTemperature(22.0f);
        entity.setMode("ON");

        return Arrays.asList(mapper.toDTO(entity));
    }

    @Override
    public ThermostatDTO getThermostat(String id) {
        DummyThermostat entity = new DummyThermostat();
        entity.setId(java.util.UUID.randomUUID().toString());
        entity.setName("Dummy thermostat");
        entity.setCurrentTemperature(17.5f);
        entity.setTargetTemperature(20.0f);
        entity.setMode("ECO");

        return mapper.toDTO(entity);
    }
}
