package nl.wilbrink.thermostat.impl.nest;

import nl.wilbrink.thermostat.dto.ThermostatDTO;
import nl.wilbrink.thermostat.ThermostatService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NestThermostatService implements ThermostatService {

    @Override
    public List<ThermostatDTO> getThermostats() {
        return null;
    }

    @Override
    public ThermostatDTO getThermostat(String id) {
        return null;
    }

}
