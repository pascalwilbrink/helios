package nl.wilbrink.config.service;

import nl.wilbrink.config.ConfigItemKey;
import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.config.entity.ConfigItem;
import nl.wilbrink.config.mapper.ConfigItemMapper;
import nl.wilbrink.config.repository.ConfigItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConfigService {

    private final ConfigItemRepository configItemRepository;

    private final ConfigItemMapper configItemMapper;

    @Autowired
    public ConfigService(ConfigItemRepository configItemRepository, ConfigItemMapper configItemMapper) {
        this.configItemRepository = configItemRepository;
        this.configItemMapper = configItemMapper;
    }

    public List<ConfigItemDTO> getAllConfigItems() {
        List<ConfigItemDTO> configItems = new ArrayList<>();

        configItemRepository.findAll().forEach(configItem -> {
            configItems.add(configItemMapper.toDTO(configItem));
        });

        return configItems;
    }

    public ConfigItemDTO getConfigItemById(String id) {
        return configItemMapper.toDTO(configItemRepository.findOne(id));
    }

    public ConfigItemDTO getConfigItemByKey(ConfigItemKey key) {
        return configItemMapper.toDTO(configItemRepository.findOneByKey(key.name()));
    }

    public ConfigItemDTO updateConfigItem(ConfigItemDTO configItem) {

        ConfigItem entity = configItemMapper.toEntity(configItem);

        entity = configItemRepository.save(entity);

        return configItemMapper.toDTO(entity);
    }
}
