package nl.wilbrink.thermostat.impl.dummy;

import nl.wilbrink.thermostat.dto.ThermostatDTO;
import org.mapstruct.Mapper;

@Mapper
public interface DummyThermostatMapper {

    ThermostatDTO toDTO(DummyThermostat entity);

    DummyThermostat toEntity(ThermostatDTO dto);

}
