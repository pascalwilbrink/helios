package nl.wilbrink.config;

public enum ConfigItemKey {
    THERMOSTAT_TYPE,
    TEMPERATURE_UNIT
}
