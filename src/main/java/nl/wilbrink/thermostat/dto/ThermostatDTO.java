package nl.wilbrink.thermostat.dto;

public class ThermostatDTO {

    private String id;

    private String name;

    private float currentTemperature;

    private float targetTemperature;

    private Mode mode;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCurrentTemperature(float currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public float getCurrentTemperature() {
        return currentTemperature;
    }

    public void setTargetTemperature(float targetTemperature) {
        this.targetTemperature = targetTemperature;
    }

    public float getTargetTemperature() {
        return targetTemperature;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public Mode getMode() {
        return mode;
    }

    public enum Mode {
        ON,
        OFF,
        ECO
    }
}
