package nl.wilbrink.thermostat.controller;

import nl.wilbrink.thermostat.dto.ThermostatDTO;
import nl.wilbrink.thermostat.service.ThermostatManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "thermostats")
public class ThermostatController {

    private final ThermostatManagementService thermostatManagementService;

    @Autowired
    public ThermostatController(ThermostatManagementService thermostatManagementService) {
        this.thermostatManagementService = thermostatManagementService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getThermostats() {
        List<ThermostatDTO> thermostats = thermostatManagementService.getThermostats();

        return ResponseEntity.ok(thermostats);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getThermostat(@PathVariable("id") String id) {
        ThermostatDTO thermostat =  thermostatManagementService.getThermostat(id);

        return ResponseEntity.ok(thermostat);
    }
}
