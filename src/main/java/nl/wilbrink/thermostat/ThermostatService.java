package nl.wilbrink.thermostat;

import nl.wilbrink.thermostat.dto.ThermostatDTO;

import java.util.List;

public interface ThermostatService {

    List<ThermostatDTO> getThermostats();

    ThermostatDTO getThermostat(String id);

}
