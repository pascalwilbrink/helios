package nl.wilbrink.thermostat.impl.dummy;

public class DummyThermostat {

    private String id;

    private String name;

    private String mode;

    private float currentTemperature;

    private float targetTemperature;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public void setCurrentTemperature(float currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public float getCurrentTemperature() {
        return currentTemperature;
    }

    public void setTargetTemperature(float targetTemperature) {
        this.targetTemperature = targetTemperature;
    }

    public float getTargetTemperature() {
        return targetTemperature;
    }

}
