package nl.wilbrink.thermostat;

import nl.wilbrink.common.exception.WebException;
import nl.wilbrink.thermostat.impl.dummy.DummyThermostatService;
import nl.wilbrink.thermostat.impl.nest.NestThermostatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ThermostatFactory {

    private final NestThermostatService nestThermostatService;

    private final DummyThermostatService dummyThermostatService;

    private Map<ThermostatType, ThermostatService> services = new HashMap<>();

    @Autowired
    public ThermostatFactory(NestThermostatService nestThermostatService, DummyThermostatService dummyThermostatService) {
        this.nestThermostatService = nestThermostatService;
        this.dummyThermostatService = dummyThermostatService;

        this.services.put(ThermostatType.NEST, this.nestThermostatService);
        this.services.put(ThermostatType.DUMMY, this.dummyThermostatService);
    }

    public ThermostatService getService(ThermostatType type) {
        if (!this.services.containsKey(type)) {
            throw new WebException(String.format("Thermostat with type %s not found", type.name()), WebException.HttpStatus.BAD_REQUEST);
        }

        return this.services.get(type);
    }
}
