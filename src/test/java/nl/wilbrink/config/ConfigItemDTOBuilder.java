package nl.wilbrink.config;

import nl.wilbrink.config.dto.ConfigItemDTO;

public final class ConfigItemDTOBuilder {
    private String id;
    private ConfigItemKey key;
    private String value;

    private ConfigItemDTOBuilder() {
    }

    public static ConfigItemDTOBuilder aConfigItemDTO() {
        return new ConfigItemDTOBuilder();
    }

    public ConfigItemDTOBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public ConfigItemDTOBuilder withKey(ConfigItemKey key) {
        this.key = key;
        return this;
    }

    public ConfigItemDTOBuilder withValue(String value) {
        this.value = value;
        return this;
    }

    public ConfigItemDTO build() {
        ConfigItemDTO configItemDTO = new ConfigItemDTO();
        configItemDTO.setId(id);
        configItemDTO.setKey(key);
        configItemDTO.setValue(value);
        return configItemDTO;
    }
}
