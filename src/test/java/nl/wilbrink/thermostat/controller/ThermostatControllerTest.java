package nl.wilbrink.thermostat.controller;

import nl.wilbrink.thermostat.ThermostatDTOBuilder;
import nl.wilbrink.thermostat.dto.ThermostatDTO;
import nl.wilbrink.thermostat.service.ThermostatManagementService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ThermostatControllerTest {

    @Mock
    private ThermostatManagementService thermostatManagementService;

    @InjectMocks
    private ThermostatController subject;

    @Test
    public void itShouldGetAListOfThermostats() {
        ThermostatDTO thermostat1 = ThermostatDTOBuilder
                .aThermostatDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withName("Thermostat 1")
                .withCurrentTemperature(17.5f)
                .withTargetTemperature(17.8f)
                .withMode(ThermostatDTO.Mode.ON)
                .build();

        ThermostatDTO thermostat2 = ThermostatDTOBuilder
                .aThermostatDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withName("Thermostat 2")
                .withCurrentTemperature(17.8f)
                .withTargetTemperature(20.0f)
                .withMode(ThermostatDTO.Mode.ECO)
                .build();

        when(thermostatManagementService.getThermostats()).thenReturn(Arrays.asList(thermostat1, thermostat2));

        ResponseEntity response = subject.getThermostats();

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), notNullValue());
        assertThat(response.getBody(), instanceOf(List.class));

        List<ThermostatDTO> thermostats = (List)response.getBody();
        assertThat(thermostats, containsInAnyOrder(thermostat1, thermostat2));
    }

    @Test
    public void itShouldGetAThermostatById() {
        String id = java.util.UUID.randomUUID().toString();

        ThermostatDTO thermostat = ThermostatDTOBuilder
                .aThermostatDTO()
                .withId(id)
                .withName("Thermostat 1")
                .withCurrentTemperature(18f)
                .withTargetTemperature(20f)
                .withMode(ThermostatDTO.Mode.ON)
                .build();

        when(thermostatManagementService.getThermostat(id)).thenReturn(thermostat);

        ResponseEntity response = subject.getThermostat(id);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), notNullValue());
        assertThat(response.getBody(), instanceOf(ThermostatDTO.class));
        assertThat(response.getBody(), equalTo(thermostat));
    }
}