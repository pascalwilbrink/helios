package nl.wilbrink.thermostat;

import nl.wilbrink.thermostat.impl.dummy.DummyThermostatService;
import nl.wilbrink.thermostat.impl.nest.NestThermostatService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class ThermostatFactoryTest {

    @Mock
    private NestThermostatService nestThermostatService;

    @Mock
    private DummyThermostatService dummyThermostatService;

    @InjectMocks
    private ThermostatFactory subject;

    @Test
    public void itShouldReturnTheNestThermostatServiceWhenTheRequestTypeIsNest() {
        ThermostatService result = subject.getService(ThermostatType.NEST);

        assertThat(result, equalTo(nestThermostatService));
    }

    @Test
    public void itShouldReturnTheDummyThermostatServiceWhenTheRequestTypeIsDummy() {
        ThermostatService result = subject.getService(ThermostatType.DUMMY);

        assertThat(result, equalTo(dummyThermostatService));
    }
}