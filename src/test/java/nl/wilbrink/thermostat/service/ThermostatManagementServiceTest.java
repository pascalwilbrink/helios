package nl.wilbrink.thermostat.service;

import nl.wilbrink.config.ConfigItemKey;
import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.thermostat.ThermostatDTOBuilder;
import nl.wilbrink.config.service.ConfigService;
import nl.wilbrink.thermostat.dto.ThermostatDTO;
import nl.wilbrink.thermostat.ThermostatFactory;
import nl.wilbrink.thermostat.ThermostatService;
import nl.wilbrink.thermostat.ThermostatType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ThermostatManagementServiceTest {

    @Mock
    private ThermostatFactory thermostatFactory;

    @Mock
    private ConfigService configService;

    @Mock
    private ThermostatService mockedService;

    @InjectMocks
    private ThermostatManagementService subject;


    @Test
    public void itShouldGetAllThermostats() {
        ThermostatDTO thermostat1 = ThermostatDTOBuilder
                .aThermostatDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withName("Thermostat 1")
                .withCurrentTemperature(18f)
                .withTargetTemperature(20f)
                .withMode(ThermostatDTO.Mode.ON)
                .build();

        ThermostatDTO thermostat2 = ThermostatDTOBuilder
                .aThermostatDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withName("Thermostat 2")
                .withCurrentTemperature(18f)
                .withTargetTemperature(20f)
                .withMode(ThermostatDTO.Mode.ECO)
                .build();

        ConfigItemDTO configItemDTO = new ConfigItemDTO();
        configItemDTO.setValue("NEST");

        when(configService.getConfigItemByKey(ConfigItemKey.THERMOSTAT_TYPE)).thenReturn(configItemDTO);

        when(thermostatFactory.getService(ThermostatType.NEST)).thenReturn(mockedService);

        when(mockedService.getThermostats()).thenReturn(Arrays.asList(thermostat1, thermostat2));

        List<ThermostatDTO> thermostats = subject.getThermostats();

        assertThat(thermostats, containsInAnyOrder(thermostat1, thermostat2));
    }

    @Test
    public void itShouldGetAThermostatById() {
        String id = java.util.UUID.randomUUID().toString();

        ThermostatDTO thermostat = ThermostatDTOBuilder
                .aThermostatDTO()
                .withId(id)
                .withName("Thermostat 1")
                .withCurrentTemperature(18f)
                .withTargetTemperature(20f)
                .withMode(ThermostatDTO.Mode.ON)
                .build();

        ConfigItemDTO configItemDTO = new ConfigItemDTO();
        configItemDTO.setValue("DUMMY");

        when(configService.getConfigItemByKey(ConfigItemKey.THERMOSTAT_TYPE)).thenReturn(configItemDTO);

        when(thermostatFactory.getService(ThermostatType.DUMMY)).thenReturn(mockedService);

        when(mockedService.getThermostat(id)).thenReturn(thermostat);

        ThermostatDTO result = subject.getThermostat(id);

        assertThat(result, equalTo(thermostat));


    }
}