package nl.wilbrink.thermostat.service;

import nl.wilbrink.config.ConfigItemKey;
import nl.wilbrink.config.service.ConfigService;
import nl.wilbrink.thermostat.ThermostatType;
import nl.wilbrink.thermostat.dto.ThermostatDTO;
import nl.wilbrink.thermostat.ThermostatFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ThermostatManagementService {

    private final ThermostatFactory thermostatFactory;

    private final ConfigService configService;

    @Autowired
    public ThermostatManagementService(ThermostatFactory thermostatFactory, ConfigService configService) {
        this.thermostatFactory = thermostatFactory;
        this.configService = configService;
    }

    public List<ThermostatDTO> getThermostats() {
        return thermostatFactory.getService(getThermostatType()).getThermostats();
    }

    public ThermostatDTO getThermostat(String id) {
        return thermostatFactory.getService(getThermostatType()).getThermostat(id);
    }

    private ThermostatType getThermostatType() {
        return ThermostatType.valueOf(configService.getConfigItemByKey(ConfigItemKey.THERMOSTAT_TYPE).getValue());
    }
}
