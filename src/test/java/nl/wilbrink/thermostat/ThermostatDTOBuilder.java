package nl.wilbrink.thermostat;

import nl.wilbrink.thermostat.dto.ThermostatDTO;

public final class ThermostatDTOBuilder {
    private String id;
    private String name;
    private float currentTemperature;
    private float targetTemperature;
    private ThermostatDTO.Mode mode;

    private ThermostatDTOBuilder() {
    }

    public static ThermostatDTOBuilder aThermostatDTO() {
        return new ThermostatDTOBuilder();
    }

    public ThermostatDTOBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public ThermostatDTOBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public ThermostatDTOBuilder withCurrentTemperature(float currentTemperature) {
        this.currentTemperature = currentTemperature;
        return this;
    }

    public ThermostatDTOBuilder withTargetTemperature(float targetTemperature) {
        this.targetTemperature = targetTemperature;
        return this;
    }

    public ThermostatDTOBuilder withMode(ThermostatDTO.Mode mode) {
        this.mode = mode;
        return this;
    }

    public ThermostatDTO build() {
        ThermostatDTO thermostatDTO = new ThermostatDTO();
        thermostatDTO.setId(id);
        thermostatDTO.setName(name);
        thermostatDTO.setCurrentTemperature(currentTemperature);
        thermostatDTO.setTargetTemperature(targetTemperature);
        thermostatDTO.setMode(mode);
        return thermostatDTO;
    }
}
