package nl.wilbrink.config.controller;

import nl.wilbrink.config.ConfigItemDTOBuilder;
import nl.wilbrink.config.ConfigItemKey;
import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.config.service.ConfigService;
import nl.wilbrink.thermostat.ThermostatType;
import nl.wilbrink.thermostat.dto.ThermostatDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfigControllerTest {

    @Mock
    private ConfigService configService;

    @InjectMocks
    private ConfigController subject;

    @Test
    public void itShouldGetAllConfigItems() {
        ConfigItemDTO configItem1 = ConfigItemDTOBuilder
                .aConfigItemDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withKey(ConfigItemKey.THERMOSTAT_TYPE)
                .withValue(ThermostatType.NEST.name())
                .build();

        ConfigItemDTO configItem2 = ConfigItemDTOBuilder
                .aConfigItemDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withKey(ConfigItemKey.TEMPERATURE_UNIT)
                .withValue("Celcius")
                .build();

        when(configService.getAllConfigItems()).thenReturn(Arrays.asList(configItem1, configItem2));

        ResponseEntity response = subject.getAllConfigItems();

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), notNullValue());
        assertThat(response.getBody(), instanceOf(List.class));

        List<ThermostatDTO> thermostats = (List)response.getBody();
        assertThat(thermostats, containsInAnyOrder(configItem1, configItem2));
    }

    @Test
    public void itShouldGetAConfigItemById() {
        String id = java.util.UUID.randomUUID().toString();

        ConfigItemDTO configItem = ConfigItemDTOBuilder
                .aConfigItemDTO()
                .withId(id)
                .withKey(ConfigItemKey.TEMPERATURE_UNIT)
                .withValue("Celcius")
                .build();

        when(configService.getConfigItemById(id)).thenReturn(configItem);

        ResponseEntity response = subject.getConfigItem(id);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), instanceOf(ConfigItemDTO.class));
        assertThat(response.getBody(), equalTo(configItem));
    }

    @Test
    public void itShouldUpdateAConfigItem() {
        String id = java.util.UUID.randomUUID().toString();

        ConfigItemDTO configItem = ConfigItemDTOBuilder
                .aConfigItemDTO()
                .withKey(ConfigItemKey.THERMOSTAT_TYPE)
                .withValue(ThermostatType.DUMMY.name())
                .build();

        when(configService.updateConfigItem(configItem)).thenReturn(configItem);

        ResponseEntity response = subject.updateConfigItem(id, configItem);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), instanceOf(ConfigItemDTO.class));
        assertThat(response.getBody(), equalTo(configItem));
        assertThat(((ConfigItemDTO)response.getBody()).getId(), equalTo(id));
    }
}